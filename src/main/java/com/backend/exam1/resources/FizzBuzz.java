package com.backend.exam1.resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.backend.exam1.Exam1Application;
import com.backend.exam1.exceptions.NullValueMaxIterationsException;
import com.backend.exam1.services.ReadPropertiesFile;

public class FizzBuzz extends Thread {
	
	private final Logger log = LoggerFactory.getLogger(ReadPropertiesFile.class);
	
	private Integer startNumber;
	private Integer maxIterations;
	private List<String> results;
		
	public void setMaxIterations(Integer startNumber,Integer maxIterations, List<String> results) {
		this.startNumber = startNumber;
		this.maxIterations = maxIterations;
		this.results = results;
	}

	public Integer getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	public List<String> getResult() {
		return results;
	}

	public void setResult(List<String> result) {
		this.results = result;
	}

	public Integer getMaxIterations() {
		return maxIterations;
	}
	
	public void setMaxIterations(Integer maxIterations) {
		this.maxIterations = maxIterations;
	}


	@Override
	public void run() {
		log.info("Start");
		List<String> results = new ArrayList<String>();
		if (maxIterations == null) throw new NullValueMaxIterationsException("Invalid max Iterations");
		for (int i = startNumber ; i < maxIterations ; ++i) {
			String result = "";
			if (i % 3 == 0 && i % 5 == 0) {
				result = "FizzBuzz";
			} else if (i % 3 == 0 ) {
				result = "Fizz";
			} else if (i % 5 == 0) {
				result = "Buzz";
			} else {
				result = String.valueOf(i);
			}
			results.add(result);
		}
		saveResults(results);
		setResult(results);
		log.info("End: " + results);
	}
	
	/**
	 * Save results of FizzBuzz play on save-results.txt file
	 * @param result List<Strings> with results
	 */
	private void saveResults (List<String> result) {
		log.info("Start save results");
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter out = null;
		try {
			Exam1Application.lockFileSaveResults.acquire();
		    fw = new FileWriter(new File(".").getAbsoluteFile() + "\\src\\main\\outputs\\save-results.txt", true);
		    bw = new BufferedWriter(fw);
		    out = new PrintWriter(bw);
		    out.println(Instant.now().toString()+" - "+result.toString());
		    out.close();
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (InterruptedException e1) {
			log.error(e1.getMessage());
		} finally {
		    if(out != null)
			    out.close();
		    try {
		        if(bw != null)
		            bw.close();
		        if(fw != null)
		            fw.close();
		    } catch (IOException e) {
		    	log.error(e.getMessage());
		    }
		}
		Exam1Application.lockFileSaveResults.release();
		log.info("End save results");
	}
	
	
	
}
