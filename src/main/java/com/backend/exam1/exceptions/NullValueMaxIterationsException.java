package com.backend.exam1.exceptions;

public class NullValueMaxIterationsException extends RuntimeException {
  public NullValueMaxIterationsException(String errorMessage) {
      super(errorMessage);
  }
}