package com.backend.exam1.ws;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.exam1.services.ReadPropertiesFile;
import com.backend.exam1.resources.FizzBuzz;

@RestController
@RequestMapping(path="/api")
public class WebService {

	private ReadPropertiesFile configFileService;
	
	public WebService (ReadPropertiesFile configFileService) {
		this.configFileService = configFileService;
	}
	
	/**
	 * Starts a thread with FizzBuzz play.
	 * @param startNumber Start number
	 * @return List<String> result of FizzBuzz play
	 * @throws IOException
	 */
	@GetMapping(path="/play-fizzbuzz/{startNumber}")
	  public List<String> playFizzBuzzThread(@PathVariable Integer startNumber) throws IOException {
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setMaxIterations(configFileService.getMaxIterations());
		fizzBuzz.setStartNumber(startNumber);
		fizzBuzz.start();
		
		return fizzBuzz.getResult();
	}
	
}
