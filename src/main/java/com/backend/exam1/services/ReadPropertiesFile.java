package com.backend.exam1.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.backend.exam1.exceptions.NullValueMaxIterationsException;

@Service
public class ReadPropertiesFile {

	private final Logger log = LoggerFactory.getLogger(ReadPropertiesFile.class);
	
	/**
	 * Gets from config.properties file the maxIterations number. If it's not found will return 0.
	 * @return Max iterations number.
	 * @throws IOException
	 */
	public Integer getMaxIterations() throws IOException {
		Properties prop=new Properties();
		FileInputStream ip= new FileInputStream(new File(".").getAbsoluteFile() + "\\src\\main\\resources\\config.properties");
		prop.load(ip);
		Integer maxIterations = 0;
		try {
			if (prop.getProperty("maxIterations").isEmpty()) {
				throw new NullValueMaxIterationsException("Empty value MaxIterations");
			} else {
				maxIterations = Integer.valueOf(prop.getProperty("maxIterations"));
			}
		}  catch (NullValueMaxIterationsException e) {
			log.error(e.getMessage());
		} catch (NumberFormatException e) {
			log.error(e.getMessage());
		}
		return maxIterations;
	}
	
	
	
}
