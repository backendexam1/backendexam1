package com.backend.exam1;

import java.util.concurrent.Semaphore;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Exam1Application {

	public static Semaphore lockFileSaveResults = new Semaphore(1, true);

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(Exam1Application.class, args);
	}
}
