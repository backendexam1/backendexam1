package com.backend.exam1.resources;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.backend.exam1.services.ReadPropertiesFile;

class FizzBuzzTest {

	@Test
	void testRun1() throws InterruptedException {
		List<String> expectedResult = new ArrayList<>(); 
        expectedResult.add("FizzBuzz"); 
        expectedResult.add("1"); 
        expectedResult.add("2"); 
        expectedResult.add("Fizz"); 
        expectedResult.add("4");
        expectedResult.add("Buzz");
        expectedResult.add("Fizz");
        expectedResult.add("7");
        expectedResult.add("8");
        expectedResult.add("Fizz");
        expectedResult.add("Buzz");
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(11);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}

	@Test
	void testRun2() throws InterruptedException {
		List<String> expectedResult = new ArrayList<>(); 
        expectedResult.add("FizzBuzz"); 
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(1);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}
	
	@Test
	void testRun3() throws InterruptedException {
		List<String> expectedResult = new ArrayList<>();
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(0);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}

	@Test
	void testRun4() throws InterruptedException {
		List<String> expectedResult = new ArrayList<>();
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(-1);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}
	
	@Test
	void testRun5() throws InterruptedException, IOException {
		ReadPropertiesFile readPropertiesFile=mock(ReadPropertiesFile.class);
		when(readPropertiesFile.getMaxIterations()).thenReturn(20);
		List<String> expectedResult = new ArrayList<>();
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(-10);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}
	
	@Test
	void testRun6() throws InterruptedException {
		List<String> expectedResult = new ArrayList<>(); 
        expectedResult.add("FizzBuzz"); 
        expectedResult.add("1");
        
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.setStartNumber(0);
		fizzBuzz.setMaxIterations(2);
		fizzBuzz.start();
		fizzBuzz.join();
		
		assertEquals(expectedResult, fizzBuzz.getResult());
	}
}
